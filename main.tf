# Create Resource Group
resource "azurerm_resource_group" "aks" {
  name     = "${var.name_prefix}-rg"
  location = var.location
}

# Create Azure AD Application for Service Principal
resource "azuread_application" "aks" {
  name = "${var.name_prefix}-sp"
}

# Create Service Principal
resource "azuread_service_principal" "aks" {
  application_id = azuread_application.aks.application_id
}

# Generate random string to be used for Service Principal Password
resource "random_string" "password" {
  length  = 32
  special = true
}

# Create Service Principal password
resource "azuread_service_principal_password" "aks" {
  end_date             = var.aks_end_date_sp # Forever
  service_principal_id = azuread_service_principal.aks.id
  value                = random_string.password.result
}

# Create managed Kubernetes cluster (AKS)
resource "azurerm_kubernetes_cluster" "aks" {
  name                = "${var.name_prefix}-aks"
  location            = azurerm_resource_group.aks.location
  resource_group_name = azurerm_resource_group.aks.name
  dns_prefix          = var.name_prefix
  kubernetes_version  = var.k8s-version

  agent_pool_profile {
    name                = var.aks_agentpool_name
    enable_auto_scaling = var.aks_pool_autoscale
    max_pods            = var.aks_max_pods
    count               = var.aks_node_count
    vm_size             = var.aks_agent_vm_sku
    os_type             = var.aks_pool_os
    os_disk_size_gb     = var.aks_node_os_disk_size_gb

  }

  #linux_profile {
  #  admin_username = var.aks_agent_admin_user
  #  ssh_key {
  #    key_data = var.aks_public_key_data
  #  }
  #}

  network_profile {
    network_plugin     = var.aks_network_plugin
    network_policy     = var.aks_network_policy
    service_cidr       = var.aks_serviceCidr
    dns_service_ip     = var.aks_dnsServiceIP
    docker_bridge_cidr = var.aks_dockerBridgeCidr
  }

  service_principal {
    client_id     = azuread_application.aks.application_id
    client_secret = azuread_service_principal_password.aks.value
  }
}

# Initialize Helm (and install Tiller)
provider "helm" {
  install_tiller = true

  kubernetes {
    host = azurerm_kubernetes_cluster.aks.kube_config[0].host
    client_certificate = base64decode(
      azurerm_kubernetes_cluster.aks.kube_config[0].client_certificate,
    )
    client_key = base64decode(azurerm_kubernetes_cluster.aks.kube_config[0].client_key)
    cluster_ca_certificate = base64decode(
      azurerm_kubernetes_cluster.aks.kube_config[0].cluster_ca_certificate,
    )
  }
}

# Create Static Public IP Address to be used by Nginx Ingress
resource "azurerm_public_ip" "nginx_ingress" {
  name                         = "nginx-ingress-pip"
  location                     = azurerm_kubernetes_cluster.aks.location
  resource_group_name          = azurerm_kubernetes_cluster.aks.node_resource_group
  allocation_method            = "Static"
  domain_name_label            = var.name_prefix
}

# Add Kubernetes Stable Helm charts repo
data "helm_repository" "stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com"
}

# Install Nginx Ingress using Helm Chart
resource "helm_release" "nginx_ingress" {
  name       = "nginx-ingress"
  repository = "${data.helm_repository.stable.metadata[0].name}"
  chart      = "nginx-ingress"

  set {
    name  = "rbac.create"
    value = "false"
  }

  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }

  set {
    name  = "controller.service.loadBalancerIP"
    value = azurerm_public_ip.nginx_ingress.ip_address
  }
}

output "resource_group_name" {
  value = azurerm_resource_group.aks.name
}

output "kubernetes_cluster_name" {
  value = azurerm_kubernetes_cluster.aks.name
  
}
