# Set default name prefix
variable "name_prefix" {
  default = "k8s-cluster"
}
# Set default location
variable "location" {
  default = "eastus"
}
#Set end date for Service principal
variable "aks_end_date_sp" {
    default = "2299-12-30T23:00:00Z"
}
# Set k8s version
variable "k8s-version" {
  default = "1.13.10"
}
# Set pool attributes
variable "aks_agentpool_name" {
  default = "linuxpool"
}
variable "aks_pool_autoscale" {
  default = "false" 
}
variable "aks_max_pods" {
  default = "30" 
  description = "The max pods limit should be larger than 30."
}
variable "aks_node_count" {
  default = "1" 
}
variable "aks_agent_vm_sku" {
  default = "Standard_DS2_v2"  
}
variable "aks_node_os_disk_size_gb" {
  default = "30" 
}
variable "aks_pool_os" {
  default = "Linux"
}

#Set Network Profile
variable "aks_network_plugin" {
  default = "azure"
}
variable "aks_network_policy" {
  default = "azure"  
}
variable "aks_serviceCidr" {
  default = "172.0.0.0/24"
}
variable "aks_dnsServiceIP" {
  default = "172.0.0.10"  
}
variable "aks_dockerBridgeCidr" {
  default = "172.17.0.1/16" 
}



